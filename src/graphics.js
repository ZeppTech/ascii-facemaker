/*
    ASCII Facemaker, copyright (C) 2021 Adel Faure, contact@adelfaure.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
const face_limits = {
  "cloth": 20,
  "chin": 8,
  "eyebrows": 9,
  "eyes": 5,
  "noze": 12,
  "beard": 8,
  "mouth": 5,
  "hair": 10,
  "glasses": 5,
  "hat": 10,
}

const face = {
  "cloth": 0,
  "chin": 0,
  "eyebrows": 0,
  "eyes": 0,
  "noze": 0,
  "beard": 0,
  "mouth": 0,
  "hair": 0,
  "glasses": 0,
  "hat": 0,
}

function print_asset(asset){
  var asset_to_print = assets[asset];
  for (var i = 0; i < asset_to_print.length; i++){
    to_print.push(asset_to_print[i]+'\n');
  }
}

function randomize_colors(){
  mode = Math.random() > 0.5? 'night': 'day';
  dark_color_index = Math.floor(Math.random() * 8);
  bright_color_index = Math.floor(Math.random() * 8);
  if (mode == 'night') {
    bg = night_dark_colors[dark_color_index];
    fg = night_bright_colors[bright_color_index];
  } else {
    bg = day_bright_colors[bright_color_index];
    fg = night_dark_colors[dark_color_index];
  }
}

function clear(){
  for (var i in face){
    face[i] = 0;
  }
}

function randomize() {
  clear();
  for (var i in face) {
    if (i == 'beard' && Math.random() > 0.25) continue;
    if (i == 'glasses' && Math.random() > 0.33) continue;
    if (i == 'hat' && Math.random() > 0.25) continue;
    face[i] = Math.floor(Math.random() * (face_limits[i] + 1));
  }
  randomize_colors();
}

function print_face(){
  var face_to_print = assets['empty'];
  face_to_print = merge_asset(face_to_print, assets['cloth_'+face['cloth']]);
  face_to_print = merge_asset(face_to_print, assets['chin_'+face['chin']]);
  face_to_print = merge_asset(face_to_print, assets['eyebrows_'+face['eyebrows']]);
  face_to_print = merge_asset(face_to_print, assets['eyes_'+face['eyes']]);
  face_to_print = merge_asset(face_to_print, assets['mouth_'+face['mouth']]);
  face_to_print = merge_asset(face_to_print, assets['beard_'+face['beard']]);
  face_to_print = merge_asset(face_to_print, assets['noze_'+face['noze']]);
  face_to_print = merge_asset(face_to_print, assets['hair_'+face['hair']]);
  face_to_print = merge_asset(face_to_print, assets['glasses_'+face['glasses']]);
  face_to_print = merge_asset(face_to_print, assets['hat_'+face['hat']]);
  for (var i = 0; i < face_to_print.length; i++){
    to_print.push(face_to_print[i]+'\n');
  }
}
