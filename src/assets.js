/*
    ASCII Facemaker, copyright (C) 2021 Adel Faure, contact@adelfaure.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
function merge_asset(a,b) {
  let lines = [];
  let lines_to_merge = [];
  let new_asset = [];
  for (var i = 0; i < a.length; i++){
    lines.push(a[i].split(''));
    lines_to_merge.push(b[i].split(''));
  }
  for (var i = 0; i < lines.length; i++){
    for (var j = 0; j < lines[i].length; j++){
      if (lines_to_merge[i][j] != ' ') {
        lines[i][j] = lines_to_merge[i][j];
      }
      if (lines[i][j] == '░') {
        lines[i][j] = ' ';
      }
    }
  }
  for (var i = 0; i < lines.length; i++){
    new_asset.push(lines[i].join(''));
  }
  return new_asset;
};
