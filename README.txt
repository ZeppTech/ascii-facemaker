Version 1.0 2021-10-12
    _    ____   ____ ___ ___   _____                __  __       _             
   / \  / ___| / ___|_ _|_ _| |  ___|_ _  ___ ___  |  \/  | __ _| | _____ _ __ 
  / _ \ \___ \| |    | | | |  | |_ / _` |/ __/ _ \ | |\/| |/ _` | |/ / _ \ '__|
 / ___ \ ___) | |___ | | | |  |  _| (_| | (_|  __/ | |  | | (_| |   <  __/ |   
/_/   \_\____/ \____|___|___| |_|  \__,_|\___\___| |_|  |_|\__,_|_|\_\___|_|   
                     
                     
                     
       .=(((=.       
      i;'   `:i      
      !__   __!      
     (~(_)-(_)~)     
      !   n   !      
       \  -  /       
       !`---'!       
      /`-._.-'\      
 _.-~'\_/ |o\_/`~-._ 
'         |o        `

To use type commands number between brackets then press ENTER to validate
command choice.

Menus
-----

Typing [ 0 ] will start and then is use to go back to precedent menu.

Main
|
+-- [ 1 ] Face
|         |
|         +-- [ 1 ] Chin (1-9)
|         |
|         +-- [ 2 ] Noze (1-13)
|         |
|         +-- [ 3 ] Eyes (1-6)
|         |
|         +-- [ 4 ] Mouth (1-6)
|
+-- [ 2 ] Pilosity
|         |
|         +-- [ 1 ] Eyebrows (1-10)
|         |
|         +-- [ 2 ] Beard (1-9)
|         |
|         +-- [ 3 ] Hair (1-11)
|
+-- [ 3 ] Accessories
|         |
|         +-- [ 1 ] Clothes (1-21)
|         |
|         +-- [ 2 ] Glasses (1-6)
|         |
|         +-- [ 3 ] Hat (1-11)
|
+-- [ 4 ] Randomize
|
+-- [ 0 ] Options
          |
          +-- [ 1 ] Save face
          |         |
          |         +-- [ 1 ] Download as PNG file
          |         |
          |         +-- [ 2 ] Change PNG scale (1-4)
          |         |
          |         +-- [ 3 ] Copy as text
          |
          +-- [ 2 ] Change colors
          |         |
          |         +-- [ 1 ] Switch to day mode
          |         |
          |         +-- [ 2 ] Change font color (1-8)
          |         |
          |         +-- [ 3 ] Change background color (1-8)
          |         | 
          |         +-- [ 4 ] Randomize colors
          |
          +-- [ 3 ] Change font (1-4)
          |
          +-- [ 4 ] About ASCII Face Maker

---

ASCII Facemaker, copyright (C) 2021 Adel Faure, contact@adelfaure.net,
https://gitlab.com/adelfaure/ascii-facemaker

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program. If not, see https://www.gnu.org/licenses/.

I'd like to thank Cthulu for motivating me with this idea of a "Facemaker for 
the 21st century". Mistfunk, Textmode Friends, SoloDevlopment members and 
people who follow me on twitter for their warm support and encouragement.

Fonts Licenses: jgs5 and jgs9 are under SIL Open Font License 1.1, TopazPlus is 
under GPL-FE and UbuntuMono is under Ubuntu Font Licence 1.0.
